#include "led.h"
#include <QPainter>
#include <QPaintEvent>
#include <QGradientStop>

#include <QTime>

#include "ufgcolor.h"

Led::Led(QWidget *parent) : QWidget(parent)
{
    m_onColor  = QColor(0,250,0);
    m_offColor = QColor(150,150,150);

    m_status = led_spento;

    m_type = led_giallo;

}

void Led::setOnColor(const QColor &onColor)
{
    m_onColor = onColor;
}

void Led::setOffColor(const QColor &offColor)
{
    m_offColor = offColor;
}

void Led::paintEvent(QPaintEvent *event)
{
//    QRect rect = event->rect();

//    QRectF rectangle(10.0, 20.0, 80.0, 60.0);

//    QPainter painter(this);
//    painter.setPen( Qt::blue );
//    painter.drawRoundedRect(rectangle, 20.0, 15.0);

//    // Main Color
    QColor mainColor((m_status)?m_onColor:m_offColor);

    QPainter painter(this);
    //painter.setRenderHint(QPainter::Antialiasing);


    QRect fullRect=event->rect();

    int size=qMin(fullRect.height(), fullRect.width());

//    fullRect.setSize( QSize(size,size) );

//    QRect rect=fullRect.adjusted( size, size ,-size,-size);

    QPoint centro = event->rect().center();

    int raggio = (size / 2) - 4;
    QPen pen1( ufg_darkgray,  2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin );
    QPen pen2( ufg_green,     2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin );

    QRadialGradient radial( centro , raggio );

    if( m_status == led_spento )
    {
        pen2.setColor( ufg_grey );
        radial.setColorAt( 0, ufg_grey);
        radial.setColorAt( 1, ufg_grey );
    }
    else if( m_type == led_verde )
    {
        pen2.setColor( ufg_green );
        radial.setColorAt( 0, ufg_lime);
        radial.setColorAt( 1, ufg_green );
    }
    else if( m_type == led_rosso )
    {
        pen2.setColor( ufg_maroon );
        radial.setColorAt( 0, ufg_red);
        radial.setColorAt( 1, ufg_maroon );
    }
    else if( m_type == led_giallo )
    {
        pen2.setColor( ufg_goldenrod );
        radial.setColorAt( 0, ufg_yellow);
        radial.setColorAt( 1, ufg_goldenrod );
    }
    else if( m_type == led_blue )
    {
        pen2.setColor( ufg_darkblue );
        radial.setColorAt( 0, ufg_dodgerblue );
        radial.setColorAt( 1, ufg_darkblue );
    }

    painter.setPen(pen2);
    painter.setBrush( radial );
    painter.drawEllipse( centro, raggio  , raggio );

    painter.setPen(pen1);
    painter.setBrush( Qt::NoBrush );
    painter.drawEllipse(centro, raggio - 4, raggio - 4  );

    pen1.setWidth(2);
    pen1.setColor( ufg_black );
    painter.setPen(pen1);
    painter.drawEllipse(centro, raggio , raggio   );
    painter.drawEllipse(centro, raggio - 6, raggio - 6  );


}
