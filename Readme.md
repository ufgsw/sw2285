# Protocollo Comeselectro  ( 1.0.2 )

I dai vengono passati da PC a Cpu attraverso una seriale configurata a **115200 8N1**

* Il  passaggio dati in è in formato ascii
* Ogni messaggio inviato dalla CPU inizia con un carattere **$** e finisce con il carattere **CR** ( carriage return )
* Ogni messaggio inviato dal PC inizia con un carattere **:** e finisce con il carattere **CR** ( carriage return )
* E' previsto un primo parametro per identificare il tipo di messaggio, seguono poi altri parametri  separati ognuno da un **;**
* Il comando è formato da un numero da tre cifre, da **001 a 099** sono comandi per la scrittura dei dati, da **101 a 199** sono comandi per la lettura dei dati
* Come ultimo parametro viene inviato un checksum **CS** per verificare l'integrità del messaggio

---

| Comando ( PC -> CPU )                                        | Dati                                                         | Risposta                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **001** [Control](#Control ( 8bit )) ( inviato ogni 100 ms ) | **:001:control;CSCR**                                        | $101;a;b;c;d;e;f;g;CSCR<br>a =  [status](#Status   ( 16bit ))<br>b = vcap<br>c = KVolt totali<br>d = mA Grafia<br>e = mA Scopia<br>f = mA Filamento<br>g = [allarmi](#Allarmi ( 8bit)) |
| **002** Configurazione                                       | **:002;a;b;c;d;e;CSCR**<br>a = KV<br>b = Time<br>c = mAs<br>d = IFil stb<br>e = IFil lac | $102;a;b;c;d;e;CSCR<br/>a = KV<br/>b = Time<br/>c = mAs<br/>d = IFil stb<br/>e = IFil lac |
| **003** Settaggio dei Parametri<br>**103** Lettura dei Parametri | **:003;a;b;c;d;e;f;g;h;i;l;CSCR**<br>a = max KV<br>b = min KV<br>c = delta KV<br>d = Corrente massima filamento grosso<br>e = Corrente massima filamento piccolo<br>f = Tempo minimo di ritardo KV Min<br>g = Percentuale ok KV<br>h = Percentuale non ok KV<br>i = Tensione condensatori ok<br>l = Tensione condensatori non ok<br>**:103;CSCR** | $103;a;b;c;d;e;f;g;h;i;l;CSCR<br/>a = max KV<br/>b = min KV<br/>c = delta KV<br/>d = Corrente massima filamento grosso<br/>e = Corrente massima filamento piccolo<br/>f = Tempo minimo di ritardo KV Min<br/>g = Percentuale ok KV<br>h = Percentuale non ok KV<br/>i = Tensione condensatori ok<br/>l = Tensione condensatori non ok<br> |
| **004** Calibrazione KV+<br>**104** Lettura dati KV+         | **:004;a;b;CSCR**<br>a = punti a zero<br>b = coefficiente di calibrazione<br>**:104;CSCR** | $104;a;b;c;d<br>a = Valore letto Kv+<br>b = Punti<br>c = punti a zero<br>d= coefficiente  di calibrazione |
| **005** Calibrazione KV -<br>**105** Lettura dati KV -       | **:005;a;b;CSCR**<br/>a = punti a zero<br/>b = coefficiente di calibrazione<br>**:105;CSCR** | $105;a;b;c;d<br/>a = Valore letto Kv-<br/>b = Punti<br/>c = punti a zero<br/>d= coefficiente  di calibrazione |
| **006** Calibrazione mA Grafia<br>**106** Lettura dati ma GRafia | **:006;a;b;CSCR**<br/>a = punti a zero<br/>b = coefficiente di calibrazione<br>**:106;CSCR** | $106;a;b;c;d<br/>a = Valore letto in mA<br/>b = Punti<br/>c = punti a zero<br/>d= coefficiente  di calibrazione |
| **007** Calibrazione mA Scopia<br>**107** Lettura dati mA Scopia | **:007;a;b;CSCR**<br/>a = punti a zero<br/>b = coefficiente di calibrazione<br>**:107;CSCR** | $107;a;b;c;d<br/>a = Valore letto in mA<br/>b = Punti<br/>c = punti a zero<br/>d= coefficiente  di calibrazione |
| **008** Calibrazione corrente di filamento<br>**108** Lettura dati Corrente di filamento | **:008;a;b;CSCR**<br/>a = punti a zero<br/>b = coefficiente di calibrazione<br>**:108;CSCR** | $108;a;b;c;d<br/>a = Valore letto in mA<br/>b = Punti<br/>c = punti a zero<br/>d= coefficiente  di calibrazione |
| **009** Settaggio IF<br>**109** Lettura IF                   | **:009;a;b;c;d;CSCR**<br>a = Coefficiente Kv dac<br/>b = Coefficiente mA dac<br/>c = IFa<br>d = IFb<br>**:109;CSCR** | $109;a;b;c;d;CSCR<br/>a = Coefficiente Kv dac<br/>b = Coefficiente mA dac<br/>c = IFa<br/>d = IFb<br/> |
| **099** Settaggio del Serial numnber<br>**199** Lettura del Serial Number | **:099;Serialnumber;CSCR**<br>**:199;CSCR**                  | $199:a;b;c;CSCR<br/>a = Serial number<br/>b = Release firmware<br/>c = Release hardware |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
| **130** Lettura dei dati grezzi analogici                    | **:130;CSCR**                                                | $130;a;b;c;d;e;f;g,h;i;l;CSCR<br>a = Valore letto KV+<br>b = Punti letti KV+<br>c = Valore letto KV-<br>d = Punti letti KV-<br>e = Valore letto mA Grafia<br>f = Punti letti Grafia<br>g = Valore letto mA Scopia<br>h = Punti letti Scopia<br>i = Valore letto IFil<br>l = Punti letti IFil |
| **998** Ripristino dei dati di calibrazione di default       | **:998;CSCR**                                                | $998;1;CSCR                                                  |
| **999** Salvataggio dei dati di calibrazione                 | **:999;CSCR**                                                | $999;1;CSCR                                                  |





#### Control ( 8bit )

---

* BIT 0   **Pre Rx**
* BIT 1   **Rx**
* BIT 2   **Reset Alarm**
* BIT 3   **Start rotazione**
* BIT 4   **Carica condensatori**
* BIT 5   **Fuoco piccolo**
* BIT 6   - 14 **Optional**
* BIT 15   **Calibrazione**



#### Status   ( 16bit )

---

* BIT0	**Pre rx**
* BIT1   **Rx**
* BIT2   **Hw Rx**
* BIT3   **Door close**
* BIT 4   **Kv 75%**
* BIT 5   **Starter ok**
* BIT 6   **Capacitor ok**
* BIT 7   **Filamento ok**
* BIT 8   **Fuoco piccolo**
* BIT 9   **Inverter ok**
* BIT 10  **Alarm**
* BIT 11 - 15 **Optional**



#### Allarmi ( 8bit)

---

* BIT 0	**KV Max**
* BIT 1    **KV Min**
* BIT 2   **Delta KV**
* BIT 3   **IA**
* BIT 4   **IG**
* BIT 5   **Fil**
* BIT 6   **Termico**
* BIT 7   **Comunicazione**
* BIT 8   **Memoria dati**
* BIT 9  **Memoria dati di calibrazione**
* BIT 10  **Alimentazione**
* BIT 11 - 15  **Optional**





#### Calcolo del checksum

```c++
unsigned char MainWindow::CRC8(unsigned char *data, unsigned char len)
{
    unsigned char crc = 0x00;
    unsigned char tempI;
    unsigned char extract;
    unsigned char sum;

    while (len--)
    {
        extract = *data++;
        for (tempI = 8; tempI; tempI--)
        {
            sum = (crc ^ extract) & 0x01;
            crc >>= 1;
            if (sum)
            {
                crc ^= 0x8C;
            }
            extract >>= 1;
        }
    }
    return crc;
}
```

