#ifndef LED_H
#define LED_H

#include <QObject>
#include <QWidget>

typedef enum
{
    led_verde,
    led_rosso,
    led_giallo,
    led_blue
}ledtype_e;

typedef enum
{
    led_spento,
    led_acceso
}ledstatus_e;


class Led : public QWidget
{
    Q_OBJECT

public:
    explicit Led( QWidget *parent=nullptr);

    void setStatus(int v)
    {
        if( v == 0  ) m_status = led_spento;
        else          m_status = led_acceso;
        repaint();
    }

    void setType( ledtype_e t) { m_type = t; }

    void setOnColor(const QColor &onColor);

    void setOffColor(const QColor &offColor);

public slots:

private:
    int    _diameter;
    QColor m_onColor;
    QColor m_offColor;

    ledstatus_e m_status;

    ledtype_e m_type;

protected:
    void paintEvent(QPaintEvent *event);

};

#endif // LED_H
