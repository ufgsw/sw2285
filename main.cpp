#include "mainwindow.h"

#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    QFile file(":style.css");
    if( file.exists() )
    {
        file.open(QFile::ReadOnly);
        QString styleSheet = QLatin1String(file.readAll());
        file.close();

        qApp->setStyleSheet( styleSheet );
    }

    w.show();
    return a.exec();
}
