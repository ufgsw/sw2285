#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    seriale = new QSerialPort(this);

    QList<QSerialPortInfo> listaCom = QSerialPortInfo::availablePorts();
    foreach ( QSerialPortInfo port, listaCom )
    {
        {
            QString loc  = port.systemLocation();
            QString des  = port.description();
            QString nome = port.portName();
            ui->comboPortName->addItem(des + " " + nome);
        }
    }

    QTimer* timer = new QTimer(this);
    QTimer* timerui = new QTimer(this);

    connect( timer, &QTimer::timeout, this, &MainWindow::tick_user );
    connect( timerui, &QTimer::timeout, this, &MainWindow::update_ui );
    connect( seriale, &QSerialPort::readyRead, this, &MainWindow::read_data );

    ledGrigio = QPixmap(":/png/led_off.png");
    ledRosso  = QPixmap(":/png/led_rosso.png");
    ledVerde  = QPixmap(":/png/led_on.png");
    ImgConnected    = QPixmap(":/png/connected_50px.png");
    ImgDisconnected = QPixmap(":/png/disconnected_50px.png");

    timer->start(    50 );
    timerui->start( 500 );

    clear_data();

    ui->ledAlarm->setType( led_rosso );
    ui->ledAlarmCanbus->setType( led_rosso );
    ui->ledAlarmDeltaKv->setType( led_rosso );
    ui->ledAlarmIA->setType( led_rosso );
    ui->ledAlarmIFIL->setType( led_rosso );
    ui->ledAlarmIG->setType( led_rosso );
    ui->ledAlarmKvMax->setType( led_rosso );
    ui->ledAlarmKvmin->setType( led_rosso );
    ui->ledAlarmTermico->setType( led_rosso );
    ui->ledAlarmFramDati->setType( led_rosso );
    ui->ledAlarmFramCalib->setType( led_rosso );
    ui->ledAlarmAlimentazione->setType( led_rosso );

    ui->ledStarterOk->setType( led_verde );
    ui->ledRx->setType( led_verde );
    ui->ledPreRx->setType( led_verde );
    ui->ledKv75->setType( led_verde );
    ui->ledInverterOk->setType( led_verde );
    ui->ledInpHwRx->setType( led_verde );
    ui->ledFuocoPiccolo->setType( led_verde );
    ui->ledFilamentoOk->setType( led_verde );
    ui->ledDoorClose->setType( led_verde );
    ui->ledCapacitorOk->setType( led_verde );
    ui->ledAlarm->setType( led_rosso );

    timeout_rx = 0;

    data.vcap = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::tick_user()
{
    static int tt = 0;

    if( seriale->isOpen() == false ) return;

    if( timeout_rx ) --timeout_rx;

    QString tx;
    QByteArray arr;

    if(     flag.b.set_max_kv )         { flag.b.set_max_kv  = 0;        tx = QString(":010;%1;").arg( ui->spinMaxKV->value() ); }
    else if(flag.b.set_min_kv )         { flag.b.set_min_kv = 0;         tx = QString(":011;%1;").arg( ui->spinMinKV->value() ); }
    else if(flag.b.set_delta_kv )       { flag.b.set_delta_kv = 0;       tx = QString(":012;%1;").arg( ui->spinDeltaKV->value() ); }
    else if(flag.b.set_ifil_max_fg )    { flag.b.set_ifil_max_fg = 0;    tx = QString(":019;%1;").arg( ui->spinIfilMaxFg->value() ); }
    else if(flag.b.set_ifil_max_fp )    { flag.b.set_ifil_max_fp = 0;    tx = QString(":020;%1;").arg( ui->spinIfilMaxFp->value() ); }
    else if(flag.b.set_percento_ok_kv ) { flag.b.set_percento_ok_kv = 0; tx = QString(":021;%1;").arg( ui->spinPercOkKv->value() ); }
    else if(flag.b.set_serial_number )  { flag.b.set_serial_number = 0;  tx = QString(":099;%1;").arg( ui->spinSerialNumber->value() ); }

    else if(flag.b.set_all_settings)
    {
        flag.b.set_all_settings = 0;
        tx = QString(":002;%1;%2;%3;%4;%5;")
                .arg( ui->spinSetKV->value() )
                .arg( ui->spinSetTime->value() )
                .arg( ui->spinSetMas->value() )
                .arg( ui->spinSetIfilStb->value() )
                .arg( ui->spinSetIfilLav->value() );
    }
    else if( flag.b.set_all_param )
    {
        flag.b.set_all_param = 0;
        tx =  QString( ":003;%1;%2;%3;%4;%5;%6;%7;%8;%9;%10;")
                .arg( ui->spinMaxKV->value() )
                .arg( ui->spinMinKV->value() )
                .arg( ui->spinDeltaKV->value() )
                .arg( ui->spinIfilMaxFg->value() )
                .arg( ui->spinIfilMaxFp->value() )
                .arg( ui->spinRitardoKVMin->value() )
                .arg( ui->spinPercOkKv->value() )
                .arg( ui->spinPercNoOkKv->value() )
                .arg( ui->spinVCapOk->value() )
                .arg( ui->spinVCapNOOk->value() );

//        flag.b.set_all_param = 0;
//        tx = QString(":003;%1;%2;%3;%4;%5;%6;%7;%8;%9;%10;%11;%12;")
//                .arg( ui->spinMaxKV->value() )
//                .arg( ui->spinMinKV->value() )
//                .arg( ui->spinDeltaKV->value() )
//                .arg( ui->spinZeroKV->value() )
//                .arg( ui->spinFsKV->value() )
//                .arg( ui->spinZeroMa->value() )
//                .arg( ui->spinFsMA->value() )
//                .arg( ui->spinZeroIfil->value() )
//                .arg( ui->spinFsIfil->value() )
//                .arg( ui->spinIfilMaxFg->value() )
//                .arg( ui->spinIfilMaxFp->value() )
//                .arg( ui->spinPercOkKv->value() );
    }
    else if( flag.b.set_all_dac )
    {
        flag.b.set_all_dac = 0;
        tx = QString(":009;%1;%2;%3;%4;")
                .arg( ui->spinCoefKvDac->value() )
                .arg( ui->spinCoefMaDac->value() )
                .arg( ui->spinIFA->value() )
                .arg( ui->spinIFD->value() );
    }
    else if(  flag.b.set_all_kv_p )
    {
        flag.b.set_all_kv_p = 0;
        tx = QString(":004;%1;%2;")
                .arg( ui->spinPuntiZeroKvp->value() )
                .arg( ui->spinCoefKvp->value() );
    }
    else if(  flag.b.set_all_kv_m )
    {
        flag.b.set_all_kv_m = 0;
        tx = QString(":005;%1;%2;")
                .arg( ui->spinPuntiZeroKvm->value() )
                .arg( ui->spinCoefKvm->value() );
    }
    else if( flag.b.set_all_grafia )
    {
        flag.b.set_all_grafia = 0;
        tx = QString(":006;%1;%2;")
                .arg( ui->spinPuntiZeroGrafia->value() )
                .arg( ui->spinCoefGrafia->value());
    }
    else if( flag.b.set_all_scopia )
    {
        flag.b.set_all_scopia = 0;
        tx = QString(":007;%1;%2;")
                .arg( ui->spinPuntiZeroScopia->value())
                .arg( ui->spinCoefScopia->value() );
    }
    else if( flag.b.set_all_ifill )
    {
        flag.b.set_all_ifill = 0;
        tx = QString(":008;%1;%2;")
                .arg( ui->spinPuntiIFillZero->value() )
                .arg( ui->spinCoefIFill->value() );
    }
    else if( flag.b.get_all_param )
    {
        flag.b.get_all_param = 0;
        tx = ":103;";
    }
    else if( flag.b.get_all_kv_p )
    {
        flag.b.get_all_kv_p = 0;
        tx = ":104;";
    }
    else if( flag.b.get_all_kv_m )
    {
        flag.b.get_all_kv_m = 0;
        tx = ":105;";
    }
    else if( flag.b.get_all_grafia )
    {
        flag.b.get_all_grafia = 0;
        tx = ":106;";
    }
    else if( flag.b.get_all_scopia )
    {
        flag.b.get_all_scopia = 0;
        tx = ":107;";
    }
    else if( flag.b.get_all_ifill )
    {
        flag.b.get_all_ifill = 0;
        tx = ":108;";
    }
    else if( flag.b.get_all_dac )
    {
        flag.b.get_all_dac = 0;
        tx = ":109;";
    }
    else if( flag.b.get_serial_number)
    {
        flag.b.get_serial_number = 0;
        tx = ":199;";
    }
    else if( flag.b.exe_save_all )
    {
        flag.b.exe_save_all = 0;
        tx = ":999;";
    }
    else if( flag.b.exe_restore_default )
    {
        flag.b.exe_restore_default = 0;
        tx = ":998;";
    }

    else
    {
        if( ui->tabWidget->currentIndex() == 2 )
        {
            if( tt == 1 ) { tx = QString(":130;"); tt = 0; }
            else          { tx = QString(":001;%1;").arg( control.val ); tt = 1; }
        }
        else
        {
            tx = QString(":001;%1;").arg( control.val );
        }
    }

    arr = tx.toLatin1();

    unsigned char crc   = CRC8( (unsigned char*)arr.data() , arr.count() );
    unsigned char crc_h = crc >> 4;
    unsigned char crc_l = crc & 0x0f;

    if (     crc_h  < 10) arr.append(crc_h + 0x30);
    else if (crc_h == 10) arr.append( 'A' );
    else if (crc_h == 11) arr.append( 'B' );
    else if (crc_h == 12) arr.append( 'C' );
    else if (crc_h == 13) arr.append( 'D' );
    else if (crc_h == 14) arr.append( 'E' );
    else if (crc_h == 15) arr.append( 'F' );

    if (     crc_l <  10) arr.append( crc_l + 0x30 );
    else if (crc_l == 10) arr.append( 'A' );
    else if (crc_l == 11) arr.append( 'B' );
    else if (crc_l == 12) arr.append( 'C' );
    else if (crc_l == 13) arr.append( 'D' );
    else if (crc_l == 14) arr.append( 'E' );
    else if (crc_l == 15) arr.append( 'F' );

    //arr.append('*');
    //arr.append('*');

    arr.append( 0x0d );

    seriale->write( arr );
}

#define rosso "color: rgb(255, 0, 0);"
#define nero  "color: rgb(0, 0, 0);"

void MainWindow::update_ui()
{
    if( timeout_rx == 0 )
    {
        clear_data();

        ui->labelConnected->setPixmap( ImgDisconnected );

        ui->buttonControlCaricaCondensatori->setChecked(false);
        ui->buttonControlFuocoPiccolo->setChecked(false);
        ui->buttonControlPreRx->setChecked(false);
        ui->buttonControlResetAllarmi->setChecked(false);
        ui->buttonControlRx->setChecked(false);
        ui->buttonControlStartRotazione->setChecked(false);

        ui->buttonCalibrazione->setChecked( false );
        ui->buttonCalibrazione2->setChecked( false );

        ui->buttonControlResetAllarmi->setChecked( false );
    }
    else
        ui->labelConnected->setPixmap( ImgConnected );

    if( param.ifa != ui->spinIFA->value() ) ui->spinIFA->setStyleSheet( rosso );
    else                                    ui->spinIFA->setStyleSheet( nero );
    if( param.ifd != ui->spinIFD->value() ) ui->spinIFD->setStyleSheet( rosso );
    else                                    ui->spinIFD->setStyleSheet( nero );


    if( param.max_kv != ui->spinMaxKV->value() ) ui->spinMaxKV->setStyleSheet( rosso );
    else                                         ui->spinMaxKV->setStyleSheet( nero );
    if( param.min_kv != ui->spinMinKV->value() ) ui->spinMinKV->setStyleSheet( rosso );
    else                                         ui->spinMinKV->setStyleSheet( nero );
    if( param.delta_kv != ui->spinDeltaKV->value() ) ui->spinDeltaKV->setStyleSheet( rosso );
    else                                             ui->spinDeltaKV->setStyleSheet( nero );
    if( param.ritardo_kv_min != ui->spinRitardoKVMin->value() ) ui->spinRitardoKVMin->setStyleSheet( rosso );
    else                                                        ui->spinRitardoKVMin->setStyleSheet( nero );
    if( param.ifil_max_fg != ui->spinIfilMaxFg->value() ) ui->spinIfilMaxFg->setStyleSheet( rosso );
    else                                                  ui->spinIfilMaxFg->setStyleSheet( nero );
    if( param.ifil_max_fp != ui->spinIfilMaxFp->value() ) ui->spinIfilMaxFp->setStyleSheet( rosso );
    else                                                  ui->spinIfilMaxFp->setStyleSheet( nero );
    if( param.percentuale_ok_kv != ui->spinPercOkKv->value() ) ui->spinPercOkKv->setStyleSheet( rosso );
    else                                                       ui->spinPercOkKv->setStyleSheet( nero );
    if( param.percentuale_non_ok_kv != ui->spinPercNoOkKv->value() ) ui->spinPercNoOkKv->setStyleSheet( rosso );
    else                                                             ui->spinPercNoOkKv->setStyleSheet( nero );
    if( param.vcap_ok != ui->spinVCapOk->value() ) ui->spinVCapOk->setStyleSheet( rosso );
    else                                           ui->spinVCapOk->setStyleSheet( nero );
    if( param.vcap_non_ok != ui->spinVCapNOOk->value() ) ui->spinVCapNOOk->setStyleSheet( rosso );
    else                                                 ui->spinVCapNOOk->setStyleSheet( nero );
    if( param.coef_kv_dac != ui->spinCoefKvDac->value() ) ui->spinCoefKvDac->setStyleSheet( rosso );
    else                                                  ui->spinCoefKvDac->setStyleSheet( nero );
    if( param.coef_ma_dac != ui->spinCoefMaDac->value() ) ui->spinCoefMaDac->setStyleSheet( rosso );
    else                                                  ui->spinCoefMaDac->setStyleSheet( nero );



    if( param.max_kv != ui->spinMaxKV->value() ) ui->spinMaxKV->setStyleSheet( rosso );
    else                                         ui->spinMaxKV->setStyleSheet( nero );
    if( settings.kv != ui->spinSetKV->value() )  ui->spinSetKV->setStyleSheet( rosso );
    else                                         ui->spinSetKV->setStyleSheet( nero );
    if( settings.time != ui->spinSetTime->value() ) ui->spinSetTime->setStyleSheet( rosso );
    else                                            ui->spinSetTime->setStyleSheet( nero );
    if( settings.mAs != ui->spinSetMas->value() )   ui->spinSetMas->setStyleSheet( rosso );
    else                                            ui->spinSetMas->setStyleSheet( nero );
    if( settings.ifil_stb != ui->spinSetIfilStb->value() ) ui->spinSetIfilStb->setStyleSheet( rosso );
    else                                                   ui->spinSetIfilStb->setStyleSheet( nero );
    if( settings.ifil_lav != ui->spinSetIfilLav->value() ) ui->spinSetIfilLav->setStyleSheet( rosso );
    else                                                   ui->spinSetIfilLav->setStyleSheet( nero );


    if( KVp.coef != ui->spinCoefKvp->value() ) ui->spinCoefKvp->setStyleSheet( rosso );
    else                                       ui->spinCoefKvp->setStyleSheet( nero );
    if( KVp.punti_zero != ui->spinPuntiZeroKvp->value() ) ui->spinPuntiZeroKvp->setStyleSheet( rosso );
    else                                                  ui->spinPuntiZeroKvp->setStyleSheet( nero );

    if( KVn.coef != ui->spinCoefKvm->value() ) ui->spinCoefKvm->setStyleSheet( rosso );
    else                                       ui->spinCoefKvm->setStyleSheet( nero );
    if( KVn.punti_zero != ui->spinPuntiZeroKvm->value() ) ui->spinPuntiZeroKvm->setStyleSheet( rosso );
    else                                                  ui->spinPuntiZeroKvm->setStyleSheet( nero );

    if( IFilamento.coef != ui->spinCoefIFill->value() ) ui->spinCoefIFill->setStyleSheet( rosso );
    else                                           ui->spinCoefIFill->setStyleSheet( nero );
    if( IFilamento.punti_zero != ui->spinPuntiIFillZero->value() ) ui->spinPuntiIFillZero->setStyleSheet( rosso );
    else                                                      ui->spinPuntiIFillZero->setStyleSheet( nero );

    if( maGrafia.coef != ui->spinCoefGrafia->value() ) ui->spinCoefGrafia->setStyleSheet( rosso );
    else                                               ui->spinCoefGrafia->setStyleSheet( nero );
    if( maGrafia.punti_zero != ui->spinPuntiZeroGrafia->value() ) ui->spinPuntiZeroGrafia->setStyleSheet( rosso );
    else                                                          ui->spinPuntiZeroGrafia->setStyleSheet( nero );

    if( maScopia.coef != ui->spinCoefScopia->value() ) ui->spinCoefScopia->setStyleSheet( rosso );
    else                                               ui->spinCoefScopia->setStyleSheet( nero );
    if( maScopia.punti_zero != ui->spinPuntiZeroScopia->value() ) ui->spinPuntiZeroScopia->setStyleSheet( rosso );
    else                                                          ui->spinPuntiZeroScopia->setStyleSheet( nero );

    ui->ledAlarm->setStatus( status.b.alarm  );
    ui->ledCapacitorOk->setStatus( status.b.capacitor_ok );
    ui->ledDoorClose->setStatus( status.b.inp_door_close );
    ui->ledFilamentoOk->setStatus( status.b.filament_ok );
    ui->ledFuocoPiccolo->setStatus( status.b.fuoco_piccolo );
    ui->ledInpHwRx->setStatus( status.b.inp_hw_rx );
    ui->ledInverterOk->setStatus(status.b.inverter_ok);
    ui->ledKv75->setStatus( status.b.kv_75 );
    ui->ledPreRx->setStatus( status.b.pre_rx );
    ui->ledRx->setStatus( status.b.rx );
    ui->ledStarterOk->setStatus( status.b.starter_ok );

    ui->ledAlarmCanbus->setStatus( alarm.b.canbus );
    ui->ledAlarmDeltaKv->setStatus( alarm.b.delta_kv );
    ui->ledAlarmIA->setStatus( alarm.b.ia );
    ui->ledAlarmIFIL->setStatus( alarm.b.fil );
    ui->ledAlarmIG->setStatus( alarm.b.ig );
    ui->ledAlarmKvMax->setStatus( alarm.b.kv_max );
    ui->ledAlarmKvmin->setStatus( alarm.b.kv_min );
    ui->ledAlarmTermico->setStatus( alarm.b.termico );
    ui->ledAlarmFramDati->setStatus( alarm.b.fram_data );
    ui->ledAlarmFramCalib->setStatus( alarm.b.fram_calib );
    ui->ledAlarmAlimentazione->setStatus( alarm.b.v_supply );

    ui->spinKVT->setValue(         data.kvt      );
    ui->spinMAGrafia->setValue(    data.maGrafia );
    ui->spinMAScopia->setValue(    data.maScopia );
    ui->spinMAFilamento->setValue( data.maFilamento );

    ui->buttonSetKVp->setEnabled( control.b.calibrazione );
    ui->buttonSetKVm->setEnabled( control.b.calibrazione );
    ui->buttonSetIFill->setEnabled( control.b.calibrazione  );
    ui->buttonSetScopia->setEnabled( control.b.calibrazione );
    ui->buttonSetGrafia->setEnabled( control.b.calibrazione );
    ui->buttonSetIF->setEnabled( control.b.calibrazione  );


    ui->spinVCAP->setValue( data.vcap );
}

void MainWindow::read_data()
{
    char ch;

    while( seriale->read(&ch,1) )
    {
        if( ch == '$' )
        {
            bufrx.clear();
            bufrx.append( ch );
        }
        else if( ch == 0x0d )
        {
            qDebug() << bufrx;
            proc_rx();
            bufrx.clear();
        }
        else
        {
            bufrx.append(ch);
        }
    }
}

void MainWindow::proc_rx()
{
    QString rx = QString( bufrx );
    QStringList listrx = rx.split( ';', Qt::SkipEmptyParts );

    if( listrx.count() == 0 ) return;

    unsigned char crc = CRC8( (unsigned char*)bufrx.data() , bufrx.count() - 2 );

    unsigned char crcrx = listrx.last().toInt(nullptr,16);

    if( crc != crcrx )
    {
        return;
    }

    timeout_rx = 20;

    int command = listrx[0].remove(0,1).toInt();

    switch (command)
    {
    case 101:
        // lettura dello status
        if( listrx.count() == 9 )
        {
            status.val       = listrx[1].toUShort();
            data.vcap        = listrx[2].toInt();
            data.kvt         = listrx[3].toInt();
            data.maGrafia    = listrx[4].toInt();
            data.maScopia    = listrx[5].toInt();
            data.maFilamento = listrx[6].toInt();
            alarm.val        = (uint16_t)listrx[7].toUInt();
        }
    break;

    case 102:
        if( listrx.count() == 7 )
        {
            settings.kv       = listrx[1].toInt();
            settings.time     = listrx[2].toInt();
            settings.mAs      = listrx[3].toInt();
            settings.ifil_stb = listrx[4].toInt();
            settings.ifil_lav = listrx[5].toInt();
        }
        break;

    case 103:
        if( listrx.count() == 12 )
        {

            param.max_kv = listrx[1].toInt();
            param.min_kv = listrx[2].toInt();
            param.delta_kv = listrx[3].toInt();
            param.ifil_max_fg = listrx[4].toInt();
            param.ifil_max_fp = listrx[5].toInt();
            param.ritardo_kv_min = listrx[6].toInt();
            param.percentuale_ok_kv = listrx[7].toInt();
            param.percentuale_non_ok_kv = listrx[8].toInt();
            param.vcap_ok = listrx[9].toInt();
            param.vcap_non_ok = listrx[10].toInt();

            ui->spinMaxKV->setValue( param.max_kv );
            ui->spinMinKV->setValue( param.min_kv );
            ui->spinDeltaKV->setValue( param.delta_kv );
            ui->spinIfilMaxFg->setValue( param.ifil_max_fg );
            ui->spinIfilMaxFp->setValue( param.ifil_max_fp );
            ui->spinRitardoKVMin->setValue( param.ritardo_kv_min );
            ui->spinPercOkKv->setValue( param.percentuale_ok_kv );
            ui->spinPercNoOkKv->setValue( param.percentuale_non_ok_kv );
            ui->spinVCapOk->setValue( param.vcap_ok );
            ui->spinVCapNOOk->setValue( param.vcap_non_ok );
        }
        break;

    case 104:
        if( listrx.count() == 6 )
        {
            KVp.valore_letto = listrx[1].toFloat() / 10;
            KVp.punti        = listrx[2].toInt();
            KVp.punti_zero   = listrx[3].toInt();
            KVp.coef         = listrx[4].toInt();

            ui->spinValoreLettoKvp->setValue( KVp.valore_letto );
            ui->spinPuntiKvp->setValue( KVp.punti );
            ui->spinPuntiZeroKvp->setValue( KVp.punti_zero );
            ui->spinCoefKvp->setValue( KVp.coef );
        }
        break;
    case 105:
        if( listrx.count() == 6 )
        {
            KVn.valore_letto = listrx[1].toFloat() / 10;
            KVn.punti        = listrx[2].toInt();
            KVn.punti_zero   = listrx[3].toInt();
            KVn.coef         = listrx[4].toInt();

            ui->spinValoreLettoKvm->setValue( KVn.valore_letto );
            ui->spinPuntiKvm->setValue( KVn.punti );
            ui->spinPuntiZeroKvm->setValue( KVn.punti_zero );
            ui->spinCoefKvm->setValue( KVn.coef );
        }
        break;
    case 106:
        if( listrx.count() == 6 )
        {
            maGrafia.valore_letto = listrx[1].toFloat() / 10;
            maGrafia.punti        = listrx[2].toInt();
            maGrafia.punti_zero   = listrx[3].toInt();
            maGrafia.coef         = listrx[4].toInt();

            ui->spinValoreLettoGrafia->setValue( maGrafia.valore_letto );
            ui->spinPuntiGrafia->setValue( maGrafia.punti );
            ui->spinPuntiZeroGrafia->setValue( maGrafia.punti_zero );
            ui->spinCoefGrafia->setValue( maGrafia.coef );
        }
        break;
    case 107:
        if( listrx.count() == 6 )
        {
            maScopia.valore_letto = listrx[1].toFloat() / 10;
            maScopia.punti        = listrx[2].toInt();
            maScopia.punti_zero   = listrx[3].toInt();
            maScopia.coef         = listrx[4].toInt();

            ui->spinValoreLettoScopia->setValue( maScopia.valore_letto );
            ui->spinPuntiScopia->setValue( maScopia.punti );
            ui->spinPuntiZeroScopia->setValue( maScopia.punti_zero );
            ui->spinCoefScopia->setValue( maScopia.coef );
        }
        break;
    case 108:
        if( listrx.count() == 6 )
        {
            IFilamento.valore_letto = listrx[1].toFloat() / 10;
            IFilamento.punti        = listrx[2].toInt();
            IFilamento.punti_zero   = listrx[3].toInt();
            IFilamento.coef         = listrx[4].toInt();

            ui->spinValoreLettoIFill->setValue( KVn.valore_letto );
            ui->spinPuntiIFill->setValue( IFilamento.punti );
            ui->spinPuntiIFillZero->setValue( IFilamento.punti_zero );
            ui->spinCoefIFill->setValue( IFilamento.coef );
        }
        break;

    case 109:
        if( listrx.count() == 6 )
        {
            param.coef_kv_dac = listrx[1].toInt();
            param.coef_ma_dac = listrx[2].toInt();
            param.ifa = listrx[3].toInt();
            param.ifd = listrx[4].toInt();

            ui->spinCoefKvDac->setValue( param.coef_kv_dac );
            ui->spinCoefMaDac->setValue( param.coef_ma_dac );
            ui->spinIFA->setValue( param.ifa );
            ui->spinIFD->setValue( param.ifd );
        }
        break;

    case 130:
        if( listrx.count() == 12 )
        {
            KVp.valore_letto      = listrx[1].toFloat() / 10;
            KVp.punti             = listrx[2].toInt();

            KVn.valore_letto      = listrx[3].toFloat() / 10;
            KVn.punti             = listrx[4].toInt();

            maGrafia.valore_letto = listrx[5].toFloat() / 10;
            maGrafia.punti        = listrx[6].toInt();

            maScopia.valore_letto = listrx[7].toFloat() / 10;
            maScopia.punti        = listrx[8].toInt();

            IFilamento.valore_letto    = listrx[9].toFloat();
            IFilamento.punti           = listrx[10].toInt();

            ui->spinValoreLettoKvp->setValue( KVp.valore_letto );
            ui->spinPuntiKvp->setValue( KVp.punti );

            ui->spinValoreLettoKvm->setValue( KVn.valore_letto );
            ui->spinPuntiKvm->setValue( KVn.punti );

            ui->spinValoreLettoGrafia->setValue( maGrafia.valore_letto );
            ui->spinPuntiGrafia->setValue( maGrafia.punti );

            ui->spinValoreLettoScopia->setValue( maScopia.valore_letto );
            ui->spinPuntiScopia->setValue( maScopia.punti );

            ui->spinValoreLettoIFill->setValue( IFilamento.valore_letto );
            ui->spinPuntiIFill->setValue( IFilamento.punti );
        }
        break;

    case 199:

        if( listrx.count() == 5 )
        {
            param.serial_number = listrx[1].toInt();
            param.release_fw    = listrx[2].toInt();
            param.release_hw    = listrx[3].toInt();

            ui->spinSerialNumber->setValue( param.serial_number );
            ui->labelRelease->setText( "Release : " + listrx[2] + " " + listrx[3] );
        }

        break;

    case 998:
        if( listrx.count() == 3 )
        {
            if( listrx.at(1).toInt() == 1 )
            {
                // Ripristino
                ui->statusBar->showMessage( "Ripristino dati di default avvenuto ", 3000 );
                this->read_all();

            }
            else
                ui->statusBar->showMessage( "Ripristino dati di default non navvenuto ", 3000 );
        }
        break;

//    case 998:
//        if( listrx.count() == 3 )
//        {
//            if( listrx.at(1).toInt() == 1 )
//            {
//                // Recupero
//                ui->statusBar->showMessage( "Recupero da fram avvenuto ", 3000 );
//                this->read_all();
//            }
//            else
//                ui->statusBar->showMessage( "Recupero da fram avvenuto ", 3000 );
//        }
//        break;

    case 999:
        if( listrx.count() == 3 )
        {
            if( listrx.at(1).toInt() == 1 )
            {
                // Salvataggio avvenuto
                ui->statusBar->showMessage( "Salvataggio avvenuto ", 3000 );
            }
            else
                ui->statusBar->showMessage( "Salvataggio non avvenuto ", 3000 );
        }
        break;

    }
}

void MainWindow::read_all()
{
    flag.b.get_all_kv_p      = 1;
    flag.b.get_all_kv_m      = 1;
    flag.b.get_all_ifill     = 1;
    flag.b.get_all_grafia    = 1;
    flag.b.get_all_scopia    = 1;
    flag.b.get_all_param     = 1;
    flag.b.get_all_dac       = 1;
    flag.b.get_serial_number = 1;


}

void MainWindow::clear_data()
{
    control.val = 0;
    status.val  = 0;
    alarm.val   = 0;
    flag.val    = 0;

    param.max_kv      = -1;
    param.min_kv      = -1;
    param.ifa         = -1;
    param.ifd         = -1;
    param.ifil_max_fg = -1;
    param.ifil_max_fp = -1;
    param.delta_kv    = -1;
    param.percentuale_ok_kv = -1;
    param.ritardo_kv_min    = -1;
    param.zero_ifil         = -1;
    param.zero_kv           = -1;
    param.coef_kv_dac       = -1;
    param.coef_ma_dac       = -1;

    settings.kv   = 40;
    settings.time = 0;
    settings.mAs  = 0;
    settings.ifil_stb = 0;
    settings.ifil_lav = 0;

    data.ia   = 0;
    data.kv   = 0;
    data.kvt  = 0;
    data.mAs  = 0;
    data.time = 0;
    data.vcap = 0;
    data.maGrafia = 0;
    data.maScopia = 0;
    data.maFilamento = 0;
}

unsigned char MainWindow::CRC8(unsigned char *data, unsigned char len)
{
    unsigned char crc = 0x00;
    unsigned char tempI;
    unsigned char extract;
    unsigned char sum;

    while (len--)
    {
        extract = *data++;
        for (tempI = 8; tempI; tempI--)
        {
            sum = (crc ^ extract) & 0x01;
            crc >>= 1;
            if (sum)
            {
                crc ^= 0x8C;
            }
            extract >>= 1;
        }
    }
    return crc;
}


void MainWindow::on_buttonOpenClose_clicked()
{
    QStringList list = ui->comboPortName->currentText().split( " " , Qt::SkipEmptyParts );

    if( seriale->isOpen() )
    {
        seriale->close();
    }
    else
    {
        seriale->setBaudRate( QSerialPort::Baud115200 );
        seriale->setParity( QSerialPort::NoParity );
        seriale->setDataBits( QSerialPort::Data8 );
        seriale->setStopBits( QSerialPort::StopBits::OneStop );
        seriale->setPortName( list.last() );

        seriale->open( QSerialPort::ReadWrite );
    }

    if( seriale->isOpen() )
    {
        ui->buttonOpenClose->setText( "CLOSE");

        this->read_all();
    }
    else
    {
        timeout_rx = 0;
        ui->buttonOpenClose->setText( "OPEN");
    }
}

void MainWindow::on_buttonRefreshSerialList_clicked()
{
    ui->comboPortName->clear();
    QList<QSerialPortInfo> listaCom = QSerialPortInfo::availablePorts();
    foreach ( QSerialPortInfo port, listaCom )
    {
        {
            QString loc  = port.systemLocation();
            QString des  = port.description();
            QString nome = port.portName();
            ui->comboPortName->addItem(des + " " + nome);
        }
    }
}

void MainWindow::on_buttonSetMaxKv_clicked()
{
    flag.b.set_max_kv = 1;
}

void MainWindow::on_buttonSetSerialNumber_clicked()
{
    flag.b.set_serial_number = 1;
}

void MainWindow::on_buttonSetSettings_clicked()
{
    flag.b.set_all_settings = 1;
}

void MainWindow::on_buttonGetParam_clicked()
{
    flag.b.get_all_param = 1;
}

void MainWindow::on_buttonControlFuocoPiccolo_toggled(bool checked)
{
    control.b.fuoco_piccolo = checked;
}

void MainWindow::on_buttonControlPreRx_toggled(bool checked)
{
    control.b.pre_rx = checked;
}

void MainWindow::on_buttonControlRx_toggled(bool checked)
{
    control.b.rx = checked;
}

void MainWindow::on_buttonControlStartRotazione_toggled(bool checked)
{
    control.b.start_rotazione = checked;
}

void MainWindow::on_buttonControlCaricaCondensatori_toggled(bool checked)
{
    control.b.carica_condensatori = checked;
}

void MainWindow::on_buttonControlResetAllarmi_toggled(bool checked)
{
    control.b.reset_alarm = checked;
}

void MainWindow::on_buttonSetKVp_clicked()
{
    flag.b.set_all_kv_p = 1;
}

void MainWindow::on_buttonSetCoefKvp_clicked()
{
    //float coef = ui->spinPuntiKvp->value()
}

void MainWindow::on_buttonSetPuntiZeroKvp_clicked()
{
    ui->spinPuntiZeroKvp->setValue( ui->spinPuntiKvp->value()   );
}

void MainWindow::on_buttonSetIF_clicked()
{
    flag.b.set_all_dac = 1;
}

void MainWindow::on_buttonSetIFill_clicked()
{
    flag.b.set_all_ifill = 1;
}

void MainWindow::on_buttonSetScopia_clicked()
{
    flag.b.set_all_scopia = 1;
}

void MainWindow::on_buttonSetGrafia_clicked()
{
    flag.b.set_all_grafia = 1;
}

void MainWindow::on_buttonSetKVm_clicked()
{
    flag.b.set_all_kv_m = 1;
}

void MainWindow::on_buttonSalvaCalibrazione_clicked()
{
    flag.b.exe_save_all = 1;
}

void MainWindow::on_buttonRileggiDatiFram_clicked()
{
    flag.b.exe_restore = 1;
}

void MainWindow::on_buttonSetDefault_clicked()
{
    flag.b.exe_restore_default = 1;
}

void MainWindow::on_buttonSetParam_clicked()
{
    flag.b.set_all_param = 1;
}


void MainWindow::on_buttonSetSerialNumber_pressed()
{

}


void MainWindow::on_buttonCalibrazione_toggled(bool checked)
{
    control.b.calibrazione = checked;
    ui->buttonCalibrazione2->setChecked( checked );
}


void MainWindow::on_buttonCalibrazione2_toggled(bool checked)
{
    control.b.calibrazione = checked;
    ui->buttonCalibrazione->setChecked( checked );
}


void MainWindow::on_buttonPuntiZeroKVP_clicked()
{
    ui->spinPuntiZeroKvp->setValue( KVp.punti );
}


void MainWindow::on_buttonCoefKvp_clicked()
{
    ui->spinCoefKvp->setValue( (ui->spinValoreSetKvp->value() * 327670) / ( KVp.punti - KVp.punti_zero) );
}


void MainWindow::on_buttonPuntiZeroKVm_clicked()
{
    ui->spinPuntiZeroKvm->setValue( KVn.punti );
}


void MainWindow::on_buttonCoefKvm_clicked()
{
    ui->spinCoefKvm->setValue( (ui->spinValoreRichiestoKvm->value() * 327670) / (KVn.punti - KVn.punti_zero )  );
}


void MainWindow::on_buttonPuntiZeroIfil_clicked()
{
    ui->spinPuntiIFillZero->setValue( IFilamento.punti );
}


void MainWindow::on_buttonCoefIFil_clicked()
{
    ui->spinCoefIFill->setValue( ( ui->spinValoreRichiestoIFill->value() * 32767) / (IFilamento.punti - IFilamento.punti_zero) );
}


void MainWindow::on_buttonPuntiZeroMaScopia_clicked()
{
    ui->spinPuntiZeroScopia->setValue( maScopia.punti );
}


void MainWindow::on_buttonCoefMaScopia_clicked()
{
    ui->spinCoefScopia->setValue( (ui->spinValoreRichiestoScopia->value() * 327670) / (maScopia.punti - maScopia.punti_zero) );
}


void MainWindow::on_buttonPuntiZeroMaGrafia_clicked()
{
    ui->spinPuntiZeroGrafia->setValue( maGrafia.punti );
}


void MainWindow::on_buttonCoefMaGrafia_clicked()
{
    ui->spinCoefGrafia->setValue( (ui->spinValoreRichiestoGrafia->value() * 327670) / (maGrafia.punti - maGrafia.punti_zero) );
}


void MainWindow::on_spinCoefKvDac_editingFinished()
{
}


void MainWindow::on_buttonCalibrazione2_clicked()
{

}

