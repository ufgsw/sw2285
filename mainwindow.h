#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>

#include "mytypedef.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QSerialPort* seriale;

    QByteArray bufrx;

    param_s      param;
    status_u     status;
    alarm_u      alarm;
    control_u    control;
    flag_s       flag;
    data_s       data;
    setting_s    settings;

    analog_s KVp;
    analog_s KVn;
    analog_s IFilamento;
    analog_s maGrafia;
    analog_s maScopia;

    QPixmap ledGrigio;
    QPixmap ledVerde;
    QPixmap ledRosso;
    QPixmap ImgConnected;
    QPixmap ImgDisconnected;

    int timeout_rx;

private slots:

    void tick_user();
    void update_ui();
    void read_data();
    void proc_rx();
    void read_all();
    void clear_data();

    unsigned char CRC8(unsigned char *data, unsigned char len);

    void on_buttonControlPreRx_toggled(bool checked);
    void on_buttonOpenClose_clicked();
    void on_buttonRefreshSerialList_clicked();
    void on_buttonSetMaxKv_clicked();
    void on_buttonSetSerialNumber_clicked();
    void on_buttonSetSettings_clicked();
    void on_buttonGetParam_clicked();
    void on_buttonControlFuocoPiccolo_toggled(bool checked);
    void on_buttonControlRx_toggled(bool checked);
    void on_buttonControlStartRotazione_toggled(bool checked);
    void on_buttonControlCaricaCondensatori_toggled(bool checked);
    void on_buttonControlResetAllarmi_toggled(bool checked);
    void on_buttonSetKVp_clicked();
    void on_buttonSetCoefKvp_clicked();
    void on_buttonSetPuntiZeroKvp_clicked();
    void on_buttonSetIF_clicked();
    void on_buttonSetIFill_clicked();
    void on_buttonSetScopia_clicked();
    void on_buttonSetGrafia_clicked();
    void on_buttonSetKVm_clicked();
    void on_buttonSalvaCalibrazione_clicked();
    void on_buttonRileggiDatiFram_clicked();
    void on_buttonSetDefault_clicked();
    void on_buttonSetParam_clicked();
    void on_buttonSetSerialNumber_pressed();
    void on_buttonCalibrazione_toggled(bool checked);
    void on_buttonCalibrazione2_toggled(bool checked);
    void on_buttonPuntiZeroKVP_clicked();
    void on_buttonCoefKvp_clicked();
    void on_buttonPuntiZeroKVm_clicked();
    void on_buttonCoefKvm_clicked();
    void on_buttonPuntiZeroIfil_clicked();
    void on_buttonCoefIFil_clicked();
    void on_buttonPuntiZeroMaScopia_clicked();
    void on_buttonCoefMaScopia_clicked();
    void on_buttonPuntiZeroMaGrafia_clicked();
    void on_buttonCoefMaGrafia_clicked();
    void on_spinCoefKvDac_editingFinished();
    void on_buttonCalibrazione2_clicked();
};
#endif // MAINWINDOW_H
