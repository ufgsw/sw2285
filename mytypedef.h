#ifndef MYTYPEDEF_H
#define MYTYPEDEF_H

#include <stdint.h>

typedef struct
{
    int max_kv;
    int min_kv;
    int delta_kv;
    int zero_kv;
    int fs_kv;
    int zero_ifil;
    int fs_ifil;
    int ifil_max_fg;
    int ifil_max_fp;
    int percentuale_ok_kv;
    int percentuale_non_ok_kv;
    int vcap_ok;
    int vcap_non_ok;

    int coef_ma_dac;
    int coef_kv_dac;

    int ritardo_kv_min;

    int serial_number;
    int release_fw;
    int release_hw;

    int ifa;
    int ifd;
}param_s;

typedef struct
{
    int kv;
    int time;
    int mAs;
    int ia;
    int vcap;

    int kvt;
    int maGrafia;
    int maScopia;
    int maFilamento;
}data_s;

typedef struct
{
    int kv;
    int time;
    int mAs;
    int ifil_stb;
    int ifil_lav;
}setting_s;

typedef union
{
    uint16_t val;
    struct
    {
        unsigned pre_rx :1;
        unsigned rx :1;
        unsigned inp_hw_rx :1;
        unsigned inp_door_close :1;
        unsigned kv_75 :1;
        unsigned starter_ok :1;
        unsigned capacitor_ok :1;
        unsigned filament_ok :1;
        unsigned fuoco_piccolo :1;
        unsigned inverter_ok :1;
        unsigned alarm :1;
        unsigned opt :4;
        unsigned calibrazione_ok :1;
    }b;
}status_u;

typedef union
{
    uint16_t val;
    struct
    {
       unsigned pre_rx :1;
       unsigned rx :1;
       unsigned reset_alarm :1;
       unsigned start_rotazione :1;
       unsigned carica_condensatori :1;
       unsigned fuoco_piccolo :1;
       unsigned opt :9;
       unsigned calibrazione :1;
    }b;
}control_u;

typedef union
{
    uint16_t val;
    struct
    {
       unsigned kv_max :1;
       unsigned kv_min :1;
       unsigned delta_kv :1;
       unsigned ia :1;
       unsigned ig :1;
       unsigned fil :1;
       unsigned termico :1;
       unsigned canbus  :1;

       unsigned fram_data  :1;
       unsigned fram_calib :1;
       unsigned v_supply   :1;

       unsigned opt :5;
    }b;
}alarm_u;

typedef union
{
    uint64_t val;
    struct
    {
        unsigned set_kv :1;
        unsigned set_time :1;
        unsigned set_mas :1;
        unsigned set_ifil_stb :1;
        unsigned set_ifil_lav :1;

        unsigned set_max_kv :1;
        unsigned set_min_kv :1;
        unsigned set_delta_kv :1;
        unsigned set_zero_kv :1;
        unsigned set_fs_kv :1;
        unsigned set_zero_ma :1;
        unsigned set_fs_ma :1;
        unsigned set_zero_ifil :1;
        unsigned set_fs_ifil :1;
        unsigned set_ifil_max_fg :1;
        unsigned set_ifil_max_fp :1;
        unsigned set_percento_ok_kv :1;
        unsigned set_serial_number :1;

        unsigned set_all_settings :1;
        unsigned set_all_param    :1;

        unsigned set_all_kv_p     :1;
        unsigned set_all_kv_m     :1;
        unsigned set_all_ifill    :1;
        unsigned set_all_scopia   :1;
        unsigned set_all_grafia   :1;
        unsigned set_all_dac       :1;

        unsigned get_all_kv_p     :1;
        unsigned get_all_kv_m     :1;
        unsigned get_all_ifill    :1;
        unsigned get_all_scopia   :1;
        unsigned get_all_grafia   :1;
        unsigned get_all_dac       :1;

        unsigned get_all_param :1;
        unsigned get_serial_number :1;
        unsigned get_all_calibration :1;

        unsigned exe_save_all :1;
        unsigned exe_restore  :1;
        unsigned exe_restore_default :1;
    }b;
}flag_s;

typedef struct
{
    float valore_letto;
    float valore_set;
    int punti;
    int punti_zero;
    int coef;
}analog_s;

#endif // MYTYPEDEF_H
